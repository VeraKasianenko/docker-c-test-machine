# FROM alpine:latest
# We will come back to alpine once they fix the unability to run sanitizers
# The issue is: the runtime 
# FROM ubuntu:latest
FROM fedora:37
COPY to-docker-image/* .

# The `ln` part is not needed on Alpine linux
RUN dnf -y install clang git binutils make python2  nasm ninja-build cmake clang-tools-extra
#RUN apt-get update && apt-get -y install clang-11 git binutils make clang-tidy-11 python2  nasm ninja-build cmake && \
    #ln -s /bin/clang-11 /bin/clang && \
    #ln -s /bin/clang-tidy-11 /bin/clang-tidy
# RUN apk update && apk upgrade && \
#   apk add bash clang-11 git binutils clang-extra-tools musl-dev make

RUN chmod 700 /*.sh

