# docker-c-test-machine

A Docker image that runs linter and compiles the code

# Testing assignments

To use this tester your repositories need to include the following files:

- `compile_flags.txt` file where all compilation flags should be placed, one per line
- `.gitlab-ci.yml` (see the [sample file](.gitlab-ci.yml.sample))


# If you want to modify it

* Change the machine name in `MACHINE_NAME` file
* The contents of `to-docker` directory will be copied to the virtual machine
  root filesystem
* Edit `Dockerfile` if necessary 
* Try to make images minimalistic but reusable e.g. include the necessary tools
  (compilers, linters, analyzers etc.) but not the tests for each assignment
   you are expecting to test using the image

